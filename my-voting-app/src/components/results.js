import React, {Component} from 'react';

class Results extends Component{
    constructor(props){
        super(props);
        this.store = this.props.store;
    }
    votesAngularInPrecent(){
        if(this.store.getState().angular){
            return (this.store.getState().angular/(this.store.getState().angular+this.store.getState().react+this.store.getState().vuejs))*100
        }
        else{
            return 0
        }
    }

    votesReactInPrecent(){
        if(this.store.getState().react){
            return (this.store.getState().react/(this.store.getState().angular+this.store.getState().react+this.store.getState().vuejs))*100
        }
        else{
            return 0
        }
    }

    votesVuejsInPrecent(){
        if(this.store.getState().react){
            return (this.store.getState().vuejs/(this.store.getState().angular+this.store.getState().react+this.store.getState().vuejs))*100
        }
        else{
            return 0
        }
    }

    votesAngularInPrecentStyle(){
        return {
            width: this.votesAngularInPrecent()+'%'
        }
    }
    votesReactInPrecentStyle(){
        return {
            width: this.votesReactInPrecent()+'%'
        }
    }
    votesVuejsInPrecentStyle(){
        return {
            width: this.votesVuejsInPrecent()+'%'
        }
    }
    


    render(){
        return(
            <div>
             <span className='label label-danger'> Angular:{this.votesAngularInPrecent().toFixed(2)+'%'}</span>
                <div className='progress progress-striped active'>
                    <div className='progress-bar progress-bar-danger' style={this.votesAngularInPrecentStyle()}>

                    </div>
                </div>
                <span className='label label-info'> React:{this.votesReactInPrecent().toFixed(2)+'%'}</span>
                <div className='progress progress-striped active'>
                    <div className='progress-bar progress-bar-info' style={this.votesReactInPrecentStyle()}>

                    </div>
                </div>
                <span className='label label-success'> Vuejs:{this.votesVuejsInPrecent().toFixed(2)+'%'}</span>
                <div className='progress progress-striped active'>
                    <div className='progress-bar progress-bar-success' style={this.votesVuejsInPrecentStyle()}>

                    </div>
                </div>
            </div>
        )
    }
}

export default Results